---
---
// Java script: need to select month, choose city, display all times and minutes for relevant dates 
var sstTable = [
{% for each in site.data.sst %} {Month: `{{ each.Month }}`, Time: `{{ each.Time }}`, Evanston_SN: `{{ each.Evanston_SN }}`, Evanston_SST: `{{ each.Evanston_SST }}`, Montreal_SN: `{{ each.Montreal_SN }}`, Montreal_SST: `{{ each.Montreal_SST }}`, Sunnyvale_SN: `{{ each.Sunnyvale_SN }}`, Sunnyvale_SST: `{{ each.Sunnyvale_SST }}`, Halifax_SN: `{{ each.Halifax_SN }}`, Halifax_SST: `{{ each.Halifax_SST }}`, Alton_SN: `{{ each.Alton_SN }}`, Alton_SST: `{{ each.Alton_SST }}`},{% endfor %}
];

var today=new Date();
const month_name = today.toLocaleString('default', { month: 'long' });
const month_old = today.getMonth();
month = (month_old + 1);
console.log("new" + month);

// Testing, run through all momisms
//
// var i = 1;
// var x = 0;
// var randforToday = 1;
// setInterval(increase, 1000);
// 
// function increase() {
//     if (i < {{ site.data.mom-csv.size }}) {
//       i++;
//       x = i;
//     }

// var x = 5;

function daysIntoYear(date){
    return (Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()) - Date.UTC(date.getFullYear(), 0, 0)) / 24 / 60 / 60 / 1000;
}

function periodatEnd(str) {
  if (typeof str !== 'undefined') {
    const result = str.search(/[\.!?;"]$/i);
    if (result === -1) {
            return str + ".";
    } else {
            return str;
    }
  }
}

// var testdate = new Date(2020, 5, 1);
// console.log(daysIntoYear(testdate));

var x = daysIntoYear(today) - 1;

//
// var randforToday = {day: 1, rand: 1,}; // Definition & example
// var randforToday = {day: 1, rand: 16,}; // Definition & mommentary
// var randforToday = {day: 1, rand: 18,}; // Definition, example, & mommentary
var today_date = month + " " + today.getDate() + ", " + today.getFullYear();

var arrayFinal = sstTable.filter( ({ Month }) => Month == month);
var fragment1 = arrayFinal[0]
var fragment2 = arrayFinal[1]
var fragment3 = arrayFinal[2]
var fragment4 = arrayFinal[3]

// Making these all into variables to be easier to handle:
// var modFinal = allMomisms[randforToday.rand - 1].momism;
var EvanstonSN = fragment1.Evanston_SN;
var MontrealSN = fragment1.Montreal_SN;
var SunnyvaleSN = fragment1.Sunnyvale_SN;
var HalifaxSN = fragment1.Halifax_SN;
var AltonSN = fragment1.Alton_SN;

var EvanstonSST1 = fragment1.Evanston_SST;
var MontrealSST1 = fragment1.Montreal_SST;
var SunnyvaleSST1 = fragment1.Sunnyvale_SST;
var HalifaxSST1 = fragment1.Halifax_SST;
var AltonSST1 = fragment1.Alton_SST;

var EvanstonSST2 = fragment2.Evanston_SST;
var MontrealSST2 = fragment2.Montreal_SST;
var SunnyvaleSST2 = fragment2.Sunnyvale_SST;
var HalifaxSST2 = fragment2.Halifax_SST;
var AltonSST2 = fragment2.Alton_SST;

var time1, time2, time3, time4;
var time1 = "noon";
var time1spans = document.getElementsByClassName("time1");
[].slice.call( time1spans ).forEach(function ( span ) {
  span.innerHTML = time1;
});

  //
// document.getElementsByClassName("time1")[0].innerHTML = time1;

if (fragment2.Time === "14:00") {
  time2 = "2 pm";
};

if (fragment2.Time === "15:00") {
  time2 = "3 pm";
};

var time2spans = document.getElementsByClassName("time2");
[].slice.call( time2spans ).forEach(function ( span ) {
  span.innerHTML = time2;
});
var time3spans = document.getElementsByClassName("time3");
var time4spans = document.getElementsByClassName("time4");

document.getElementById("month_name").innerHTML = month_name;

if (typeof fragment3 === 'undefined') {
//   [].slice.call( time3spans ).foreach(function ( span ) {
//     span.style.display = "none";
//   });
     time3 = "";
} else {
  var time3 = fragment3.Time;
  [].slice.call( time3spans ).foreach(function ( span ) {
    span.innerHTML = time3;
  });
}

if (typeof fragment4 === 'undefined') {
//   [].slice.call( time4spans ).foreach(function ( span ) {
//     span.style.display = "none";
//   });
     time4 = "";
} else {
  var time4 = fragment4.Time;
  document.getElementsByClassName("time4")[0].innerHTML = time4;
}

document.getElementById("Evanston_SN").innerHTML = EvanstonSN;
document.getElementById("Montreal_SN").innerHTML = MontrealSN;
document.getElementById("Halifax_SN").innerHTML = HalifaxSN;
document.getElementById("Sunnyvale_SN").innerHTML = SunnyvaleSN;
document.getElementById("Alton_SN").innerHTML = AltonSN;

document.getElementById("Evanston_SST1").innerHTML = EvanstonSST1;
document.getElementById("Montreal_SST1").innerHTML = MontrealSST1;
document.getElementById("Halifax_SST1").innerHTML = HalifaxSST1;
document.getElementById("Sunnyvale_SST1").innerHTML = SunnyvaleSST1;
document.getElementById("Alton_SST1").innerHTML = AltonSST1;

document.getElementById("Evanston_SST2").innerHTML = EvanstonSST2;
document.getElementById("Montreal_SST2").innerHTML = MontrealSST2;
document.getElementById("Halifax_SST2").innerHTML = HalifaxSST2;
document.getElementById("Sunnyvale_SST2").innerHTML = SunnyvaleSST2;
document.getElementById("Alton_SST2").innerHTML = AltonSST2;

// document.getElementById("linkFinal").setAttribute("href", linkFinal)
// document.getElementById("linkFinal2").setAttribute("href", linkFinal)

// }
